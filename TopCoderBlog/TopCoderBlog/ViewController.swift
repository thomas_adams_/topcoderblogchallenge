//
//  ViewController.swift
//  TopCoderBlog
//
//  Created by Thomas Adams on 16/10/15.
//  Copyright © 2015 Smudge Apps. All rights reserved.
//

import UIKit

class ViewController: UIViewController, NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource, UIViewControllerPreviewingDelegate {

    var entryTitle = ""
    var entryDescription = ""
    var entryLink = ""
    var currentParsedElement = ""
    var entryDictionary = [String:String]()
    var entriesArray = [[String: String]]()
    var entryContent = ""

    @IBOutlet var titlesTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        self.titlesTableView.estimatedRowHeight = 40.0
//        self.titlesTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")

        // Check for force touch feature, and add force touch/previewing capability.
        if traitCollection.forceTouchCapability == .Available {
            /*
            Register for `UIViewControllerPreviewingDelegate` to enable
            "Peek" and "Pop".
            (see: MasterViewController+UIViewControllerPreviewing.swift)

            The view controller will be automatically unregistered when it is
            deallocated.
            */
            registerForPreviewingWithDelegate(self, sourceView: view)
        }

        let urlString = NSURL(string: "https://www.topcoder.com/feed/?post_type=blog")
        let rssUrlRequest = NSURLRequest(URL:urlString!)

        let queue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(rssUrlRequest, queue: queue) {
            (response, data, error) -> Void in
            if let data = data?.subdataWithRange(NSRange(location: 1, length: (data?.length ?? 1) - 1)) {

                let xmlParser = NSXMLParser(data: data)
                xmlParser.delegate = self
                xmlParser.parse()
            }
        }

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    // true if we are in an item element during xml parsing
    var inItem = false

    static let itemKey = "item"
    static let titleKey = "title"
    static let descriptionKey = "description"
    static let linkKey = "link"
    static let contentKey = "content:encoded"
    //MARK: NSXMLParserDelegate
    func parser(parser: NSXMLParser,
        didStartElement elementName: String,
        namespaceURI: String?,
        qualifiedName: String?,
        attributes attributeDict: [String : String]){
            if !inItem {
                if elementName == ViewController.itemKey {
                    inItem = true
                }
            }else {
                if elementName == ViewController.titleKey {
                    entryTitle = ""
                    currentParsedElement = ViewController.titleKey
                }
                if elementName == ViewController.descriptionKey {
                    entryDescription = ""
                    currentParsedElement = ViewController.descriptionKey
                }
                if elementName == ViewController.linkKey {
                    entryLink = ""
                    currentParsedElement = ViewController.linkKey
                }
                if elementName == ViewController.contentKey {
                    currentParsedElement = elementName
                    entryContent = ""
                }
            }
    }

    func parser(parser: NSXMLParser,
        foundCharacters string: String){
            if currentParsedElement == ViewController.titleKey {
                entryTitle = entryTitle + string
            }
            if currentParsedElement == ViewController.descriptionKey {
                entryDescription = entryDescription + string
            }
            if currentParsedElement == ViewController.linkKey {
                entryLink = entryLink + string
            }
            if currentParsedElement == ViewController.contentKey {
                entryContent += string
            }
    }

    func parser(parser: NSXMLParser,
        didEndElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?){
            if inItem{
                if elementName == ViewController.itemKey {
                    inItem = false
                    entriesArray.append(entryDictionary)
                }
                if elementName == ViewController.titleKey {
                    entryDictionary[ViewController.titleKey] = entryTitle
                }
                if elementName == ViewController.linkKey{
                    entryDictionary[ViewController.itemKey] = entryLink
                }
                if elementName == ViewController.descriptionKey {
                    entryDictionary[ViewController.descriptionKey] = entryDescription
                }
                if elementName == ViewController.contentKey {
                    entryDictionary[ViewController.contentKey] = entryContent

                }
            }

    }

    func parserDidEndDocument(paser: NSXMLParser){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.titlesTableView.reloadData()
        })
    }

    // MARK: UITableViewDataSource
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{

            var cell:UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell!

            if (nil == cell){
                cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
            }
            cell!.textLabel?.text = entriesArray[indexPath.row][ViewController.titleKey]
            cell!.textLabel?.numberOfLines = 0

            cell!.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

            return cell
    }


    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int{
            return entriesArray.count
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailViewController = WebViewController()
        guard let url = NSURL(string: self.entriesArray[indexPath.row][ViewController.linkKey] ?? "") else {
            return
        }
        detailViewController.url = url
        detailViewController.html = self.entriesArray[indexPath.row][ViewController.contentKey] ?? ""

        self.navigationController?.pushViewController(detailViewController, animated: true)
    }

    /// Create a previewing view controller to be shown at "Peek".
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        // Obtain the index path and the cell that was pressed.
        guard let indexPath = self.titlesTableView.indexPathForRowAtPoint(location),
            cell = self.titlesTableView.cellForRowAtIndexPath(indexPath),
            let url = NSURL(string: self.entriesArray[indexPath.row][ViewController.linkKey] ?? "")
            else { return nil }

        let detailViewController = WebViewController()
        // Create a detail view controller and set its properties.

        /*
        Set the height of the preview by setting the preferred content size of the detail view controller.
        Width should be zero, because it's not used in portrait.
        */

        detailViewController.preferredContentSize = CGSize(width: 0.0, height: 400)
        detailViewController.url = url
        detailViewController.html = self.entriesArray[indexPath.row][ViewController.contentKey] ?? ""
        // Set the source rect to the cell frame, so surrounding elements are blurred.
        previewingContext.sourceRect = cell.frame

        return detailViewController
    }

    /// Present the view controller for the "Pop" action.
    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        // Reuse the "Peek" view controller for presentation.
        showViewController(viewControllerToCommit, sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let webViewController = segue.destinationViewController as? WebViewController,
            let indexPath =  sender as? NSIndexPath,
            let link = self.entriesArray[indexPath.row][ViewController.contentKey],
            let url = NSURL(string: self.entriesArray[indexPath.row][ViewController.contentKey] ?? ""){
                webViewController.html = link
                webViewController.url = url
        }
    }
}


import WebKit

class WebViewController :UIViewController, UIWebViewDelegate {
    // I want to use html from rss feed, but that doesn't work outside simulator so I just open the link
    var url = NSURL() // URL to open
    var html = "" //HTML I want to open but can't for mysterious reasons

    var webView: UIWebView?

    override func viewDidLoad() {
        super.viewDidLoad()
        webView = UIWebView()
        webView?.translatesAutoresizingMaskIntoConstraints = false
        webView?.delegate = self
        self.view.addSubview(webView!)

        let constraint1 = NSLayoutConstraint.constraintsWithVisualFormat("H:|[webView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["webView":webView ?? UIView()])
        let constraint2 = NSLayoutConstraint.constraintsWithVisualFormat("V:|[webView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["webView":webView ?? UIView()])

        NSLayoutConstraint.activateConstraints(constraint1 + constraint2)
        let html = "data:text/html;charset=utf-8" + "<html>" + self.html + "</html>"
        let urlString = html.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        if  let urlString = html.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding),
        let url  = NSURL(string: urlString) {
            webView?.loadRequest(NSURLRequest(URL: url))
        }
        //        webView?.loadHTMLString("<html>\(html)</html>", baseURL: nil)


        self.view.layoutIfNeeded()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    //MARK: UIWebViewDelegate
    //for debugging:
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView){
        
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?){
        print(error)
    }
    
}